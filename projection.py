#!/usr/bin/env python3

import math

# https://tools.ietf.org/html/rfc7946#page-12

# ~ latitude    = 41.145556; // (φ)
# ~ longitude   = -73.995;   // (λ)

# Berlin 13.38,52.52 => 

lon = 13.38
lat = 52.52

# ~ mapWidth    = 200;
# ~ mapHeight   = 100;
w = 1000
h = 1000

def getMecatorPicCoords( lon, lat, w=1000, h=1000 ):
	"""
	transform coordinates in a mercator projection
	for a picture with given size
	"""
	# get x value
	# ~ x = (longitude+180)*(mapWidth/360)
	x = (lon+180)*(w/360)

	# convert from degrees to radians
	# ~ latRad = latitude*PI/180;
	# latRad = lat*math.pi/180 # used only once, so not stored in a variable

	# get y value
	# def ln(x): return math.log(x) # with single argument
	# mercN = math.log(tan((math.pi/4)+(latRad/2))); # latRad integrated
	# mercN = math.log(math.tan((math.pi/4)+(lat*math.pi/90))) # integrated as used once only
	y     = (h/2)-(w*math.log(math.tan((math.pi/4)+(lat*math.pi/90)))/(2*PI)) # mercN integrated
	return (x,y)

# call the function
print( getMecatorPicCoords(x,y) )
