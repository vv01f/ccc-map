#!/usr/bin/env python3

from __future__ import print_function
from geopy import Nominatim
# ~ import json
import sys

#disable ssl verification
import ssl
import geopy.geocoders
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE
geopy.geocoders.options.default_ssl_context = ctx
#/

# debug print to stderr, https://stackoverflow.com/questions/5574702/how-to-print-to-stderr-in-python
def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

# ~ fn="ct-csv"
if len(sys.argv) == 2:
    fn = sys.argv[1]
else:
    eprint("in case you do not know what you are doing, better call via e.g. `create-geojson.sh`.\n");
    eprint("expecting one argument: csv-file.");
    eprint("received "+str(len(sys.argv))+" argument(s): "+str(sys.argv));
    sys.exit(1) # exit with error

# for retreiving geo coordinates from addresses
geolocator = Nominatim(user_agent="my-mapper")
# lat: 0=111,1km,1=11km,2=1km… lng: 0=70km,1=7km,2=.7km…
precision = 2

# convert csv to geojson

firstline = True
# start json list
print ( '{"type":"FeatureCollection","features":[' )
# get data from file
with open( fn, 'r' ) as fp:
    for place in fp:
        #error handling: expect
        arrAddress = place.split(",")
        strAddress = ",".join(arrAddress[2:6])
        eprint( "looking up: "+strAddress )
        location = geolocator.geocode( strAddress )
        if location is not None: # its a class
            # todo: ceil coords to hide true location in format string
            # ~ strCoordPlace = '{:s},{:.6f},{:.6f}'.format( arrAddress[3], location.latitude, location.longitude ) 
            geojson = '{{"type":"Feature","geometry":{{"type":"Point","coordinates":[{:.'+str(precision)+'f},{:.'+str(precision)+'f}]}},"properties":{{"name":"{:s}","marker":"{:s}"}}}}';
            strCoordPlace = geojson.format( location.longitude, location.latitude, arrAddress[1], fn.split("-")[0] ) 
            if firstline == False :
                print (",")
            else:
                firstline = False
            print ( strCoordPlace )
# end json list
print ( "]}" )

# exit fine
sys.exit(0)

# ~ m = folium.Map( # etc..)
# ~ m.save("filename.png")

# ~ https://github.com/python-visualization/folium/issues/35#issuecomment-164784086
# ~ https://github.com/wbkd/leaflet-mapshot
# ~ https://stackoverflow.com/questions/44800396/python-ipyleaflet-export-map-as-png-or-jpg-or-svg

# ~ m = folium.Map(location=[51, 13], zoom_start=5, tiles='Stamen Toner') # Mapbox Bright , CartoDB Positron
