
# ccc maps

the [svg map] used on [ccc de] is at least potentially outdated
and thus in future should be updated automatically

[svg map]: https://chaos.expert/telegnom/erfakarte 
[ccc de]: https://www.ccc.de/regional


## my target process

* [x] fetch addressed of a convenient source
* [x] resolve geo coordinates for addresses, e.&thinsp;g. [geopy]
* [x] visualize on a map, e. g. via [leafletjs] or [QGIS]
* [ ] calculate coordinates to place POI on a SVG with a known projection
* [ ] manipulate SVG so that the POI are shown accordingly even without any application
* [ ] change view port of SVG so it frames the POIs properly
* [x] create PNG from the SVG and deploy
* [ ] document for others to use


## data

### POI

the address data source is the semantic [documentation wiki], queried with [result format]

[documentation wiki]: https://doku.ccc.de/Liste_der_Erfa-Kreise_und_Chaostreffs
[result format]: https://www.semantic-mediawiki.org/wiki/Help:CSV_format

To create GeoJSON from recent address data:

1. `prepare.sh` pulls CSV from the Wiki
2. `create-geojson.sh` calls `lookup.py` for each CSV set 
3. The resulting [data](https://gitea.c3d2.de/vv01f/ccc-map/src/branch/data) in [GeoJSON] format can be used with e.g. [leafletjs] or [QGIS] 


### Map

The old [svg map] was nice for Germany only.
Over time more European spaces joined and the map could not display all of them.

Another point is that the projection parameters of the SVG are not known.
New material can be produced choosing the projection.
One recommended program to do this is [QGIS].
Freely useable data is available on e.&thinsp;g. [Natural Earth].

To create a new SVG Map:

1. Start QGIS, KBS setting recommendation: EPSG:3857 / Pseudo Mercator
2. Drop a Shape-File, e.&thinsp;g. sqlite format (and optionally a GeoJSON-File)
3. Adjust colors in the layers styles
4. Print as SVG (Label for POI data is lost in version 2.18)

[QGIS]: https://qgis.org/ "QGIS"
[Natural Earth]: http://naturalearthdata.com/ "Natural Earth"

[geopy]: https://geopy.readthedocs.io/
[leafletjs]: https://leafletjs.com/
[GeoJSON]: https://geojson.org/ "Website for RFC 7946"

<!--

## Thanks go to …

* [telegnom](https://chaos.expert/telegnom/erfakarte) for patiently describing the problems and needs for the map
* [ax3l](https://github.com/ax3l) for his [spontanious lightning talk](https://media.ccc.de/v/DS2016-7782-lightning_talks#t=5112) on [„Visualisierung Flüchtlingsfeindlicher Vorfälle in Deutschland“](https://github.com/ax3l/chronik-vorfaelle) at [Datenspuren](http://datenspuren.de/) in 2016 getting me started years later when I saw it again
* The [Dresden OSM Meeting](https://wiki.openstreetmap.org/wiki/DresdnerOSMStammtisch) for hinting me on QGIS and an introduction how to use it
* and quite some people helping me to finally polish some python

-->
