#!/usr/bin/env sh

# baerer is the `authority` as in RFC 3986, here login for doku.ccc.de
fae="file already exists."
# recv data
if test -e baerer ; then
	auth=$(head -1 baerer)
else
	echo "error: authentication information not found."
	exit
fi
#~ test -e ct-json && echo "$fae" || curl -o ct-json "https://"${auth}"doku.ccc.de/Spezial:Semantische_Suche/-5B-5BKategorie:Chaostreffs-5D-5D-20-5B-5BChaostreff-2DIs-2DErfa::falsch-5D-5D-20-5B-5BChaostreff-2DActive::wahr-5D-5D/-3FChaostreff-2DPhysical-2DAddress%3DAdresse/-3FChaostreff-2DPhysical-2DHousenumber%3DHausnummer/-3FChaostreff-2DPhysical-2DPostcode%3DPLZ/-3FChaostreff-2DPhysical-2DCity%3DStadt/-3FChaostreff-2DCountry%3DLand/mainlabel%3D/limit%3D100/order%3DASC/sort%3DErfa-2DCity/offset%3D0/format%3Djson/headers%3Dshow/searchlabel%3DJSON"
#~ test -e erfa-json && echo "$fae" || curl -o erfa-json "https://"${auth}"doku.ccc.de/Spezial:Semantische_Suche/-5B-5BKategorie:Chaostreffs-5D-5D-20-5B-5BChaostreff-2DIs-2DErfa::wahr-5D-5D-20-5B-5BChaostreff-2DActive::wahr-5D-5D/-3FChaostreff-2DPhysical-2DAddress%3DAdresse/-3FChaostreff-2DPhysical-2DHousenumber%3DHausnummer/-3FChaostreff-2DPhysical-2DPostcode%3DPLZ/-3FChaostreff-2DPhysical-2DCity%3DStadt/-3FChaostreff-2DCountry%3DLand/mainlabel%3D/limit%3D100/order%3DASC/sort%3DErfa-2DCity/offset%3D0/format%3Djson/headers%3Dshow/searchlabel%3DJSON"
if test -e ct-csv ; then
	echo "$fae"
else
	curl -k -o ct-csv "https://${auth}doku.ccc.de/Spezial:Semantische_Suche/-5B-5BKategorie:Chaostreffs-5D-5D-20-5B-5BChaostreff-2DIs-2DErfa::falsch-5D-5D-20-5B-5BChaostreff-2DActive::wahr-5D-5D/-3FChaostreff-2DCity%3DLabel/-3FChaostreff-2DPhysical-2DAddress%3DAdresse/-3FChaostreff-2DPhysical-2DHousenumber%3DHausnummer/-3FChaostreff-2DPhysical-2DPostcode%3DPLZ/-3FChaostreff-2DPhysical-2DCity%3DStadt/-3FChaostreff-2DCountry%3DLand/mainlabel%3D/limit%3D100/order%3DASC/sort%3DChaostreff-2DCity/offset%3D0/format%3Dcsv/headers%3Dshow/searchlabel%3DCSV/sep%3D,/filename%3Dct-2Dbesuchsadressen.csv"
fi
if test -e erfa-csv ; then
	echo "$fae"
else
	curl -k -o erfa-csv "https://${auth}doku.ccc.de/Spezial:Semantische_Suche/-5B-5BKategorie:Erfa-2DKreise-5D-5D-20-5B-5BChaostreff-2DActive::wahr-5D-5D/-3FChaostreff-2DCity%3DLabel/-3FChaostreff-2DPhysical-2DAddress%3DAdresse/-3FChaostreff-2DPhysical-2DHousenumber%3DHausnummer/-3FChaostreff-2DPhysical-2DPostcode%3DPLZ/-3FChaostreff-2DPhysical-2DCity%3DStadt/-3FChaostreff-2DCountry%3DLand/mainlabel%3D/limit%3D100/order%3DASC/sort%3DChaostreff-2DCity/offset%3D0/format%3Dcsv/headers%3Dshow/searchlabel%3DCSV/sep%3D,/filename%3Derfa-2Dbesuchsadressen.csv"
fi
# preprocess csv data
for file in ct-csv erfa-csv ; do
	sed -e 's/"//g' ${file} > tmpfile && mv tmpfile ${file}
	#~ sed -e 's/\([0-9]\{4,5\}\),/\1 /g' ${file} > tmpfile && mv tmpfile ${file}
	sed '1d' ${file} > tmpfile && mv tmpfile ${file}
done
# dependencies

#~ pip3 install geopy # looking up addresses/coordinates
#~ pip3 install folium # leaflet.js with python
