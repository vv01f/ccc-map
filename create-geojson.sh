#!/usr/bin/env sh
echo "this takes some time depending on the amount of addresses in your lists …"
for file in *-csv ; do
	out=$(echo "${file}"|cut -d- -f1)".geojson"
	if test -e "${out}" ; then
		echo "file exists: ${out}, skipping."
		continue
	fi
	echo ./lookup.py "${file}"
	./lookup.py "${file}" > "${out}"
	#~ ./lookup.py "${file}" 2>/dev/null > "${out}"
done
